package com.primeton.poctag.api.tagvalue;

import com.primeton.poctag.api.BaseController;
import com.primeton.poctag.core.model.ApiResponse;
import com.primeton.poctag.core.model.PageWrap;
import com.primeton.poctag.dao.tagvalue.model.Tagvalue;
import com.primeton.poctag.service.tagvalue.TagvalueService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * POC标签表接口
 * @author 李功林
 * @date 2021/08/20 13:20
 */
@RestController
@RequestMapping("/tagvalue")
public class TagvalueController extends BaseController {

    @Autowired
    private TagvalueService tagvalueService;

    /**
     * 创建
     * @author 李功林
     * @date 2021/08/20 13:20
     */
    @PostMapping("/create")
    public ApiResponse create(@RequestBody Tagvalue tagvalue) {
        return ApiResponse.success(tagvalueService.create(tagvalue));
    }

    /**
     * 用户删除
     * @author 李功林
     * @date 2021/08/20 13:20
     */
    @GetMapping("/delete/{id}")
    public ApiResponse deleteById(@PathVariable String id) {
        tagvalueService.deleteById(id);
        return ApiResponse.success(null);
    }

    /**
     * 修改用户
     * @author 李功林
     * @date 2021/08/20 13:20
     */
    @PostMapping("/updateById")
    public ApiResponse updateById(@RequestBody Tagvalue tagvalue) {
        tagvalueService.updateById(tagvalue);
        return ApiResponse.success(null);
    }

    /**
     * 分页查询
     * @author 李功林
     * @date 2021/08/20 13:20
     */
    @PostMapping("/page")
    public ApiResponse findPage (@RequestBody PageWrap<Tagvalue> pageWrap) {
        return ApiResponse.success(tagvalueService.findPage(pageWrap));
    }

    /**
     * 通过ID查询
     * @author 李功林
     * @date 2021/08/20 13:20
     */
    @GetMapping("/{id}")
    public ApiResponse finById(@PathVariable String id) {
        return ApiResponse.success(tagvalueService.findById(id));
    }
}
