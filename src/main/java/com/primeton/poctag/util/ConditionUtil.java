package com.primeton.poctag.util;

import cn.hutool.core.collection.CollectionUtil;
import com.primeton.poctag.dto.QueryDto;

import java.util.List;

public class ConditionUtil {




    /**
     * @param queryDto      查询条件
     * @param countOrAll    查询数据还是count 1:数据  2:count
     * @return
     */
    public static String createSql(List<QueryDto> queryDto, int countOrAll) {
        String sql = "";
        if (countOrAll == 1) {
            //查询数据
            sql = "select * from user_labels where 1=1";
        } else {
            //查询条数
            sql = "select count(1) from user_labels where 1=1";
        }
        if (CollectionUtil.isEmpty(queryDto)) {
            return sql;
        }
        sql = sql + " and ";
        for (QueryDto dto : queryDto) {
            String type = dto.getType();
            String attrname = dto.getAttrname();
            if (type != null) {
                String connArr = null;
                switch (attrname) {
                    case "|":
                        connArr = " or ";
                        break;
                    case "&":
                        connArr = " and ";
                        break;
                    case "(":
                        connArr = "(";
                        break;
                    case ")":
                        connArr = ")";
                        break;
                }
                sql = sql + connArr + " ";
                continue;
            }
            String columnName = dto.getFieldname();
            String condition = dto.getLogicalConnective();
            List<String> values = dto.getValues();
            switch (condition) {
                case "eq":
                    sql = sql + " " + columnName + " = '" + values.get(0) + "'";
                    break;
                case "neq":
                    sql = sql + " " + columnName + " <> '" + values.get(0) + "'";
                    break;
                case "like":
                    sql = sql + " " + columnName + " like '%" + values.get(0) + "%'";
                    break;
                case "betweenEq"://闭区间
                    sql = sql + " " + columnName + " between '" + values.get(0) + "' and '" + values.get(1) + "'";
                    break;
                case "betweenNEq"://开区间
                    break;
                case "prefix"://匹配以 columnName 为前缀的
                    sql = sql + " " + columnName + " like '" + values.get(0) + "%'";
                    break;
                case ">": //大于
                    sql = sql + " " + columnName + " > '" + values.get(0) + "'";
                    break;
                case ">=": //大于等于
                    sql = sql + " " + columnName + " >= '" + values.get(0) + "'";
                    break;
                case "<": //小于
                    sql = sql + " " + columnName + " < '" + values.get(0) + "'";
                    break;
                case "<=": //小于等于
                    sql = sql + " " + columnName + " <= '" + values.get(0) + "'";
                    break;
                case "in": //多个字段
                    sql = sql + " " + columnName + " in( " ;
                    for (int i = 0; i < values.size(); i++) {
                        sql+="'"+values.get(i)+"',";
                    }
                   sql=  sql.substring(0,sql.length()-1);
                   sql+= " )";
                    break;
                default:
                    //不支持的关键字
                    break;
            }
        }
        return sql;
    }
}
