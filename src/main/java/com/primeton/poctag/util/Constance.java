package com.primeton.poctag.util;

/**
 * <pre>
 *
 * Created by zhaopx.
 * User: zhaopx
 * Date: 2019-06-25
 * Time: 18:26
 *
 * </pre>
 *
 * @author zhaopx
 */
public class Constance {


    /**
     * bash 执行
     */
    public final static String EXE_BASH = "bash";



    /**
     * python 执行
     */
    public final static String EXE_PYTHON = "python";


    /**
     * 任务调度类型，一次性的
     */
    public final static String TYPE_ONCE = "once";

    /**
     * 任务调度类型，crontab 表达式
     */
    public final static String TYPE_CRON = "cron";

    /**
     * 任务调度类型，固定周期
     */
    public final static String TYPE_PERIOD = "period";

    /**
     * 任务调度类型，被动调度
     */
    public final static String TYPE_PASSIVE = "passive";


    /**
     * Spark Master Local
     */
    public final static String SPARK_MASTER_PREFIX_LOCAL = "local";


    /**
     * 前端，使用默认 kafka groupId
     */
    public final static String DEFAULD_GROUPID = "0";


    /**
     * 前端，使用生成新的 kafka groupId
     */
    public final static String NEW_GROUPID = "1";


    /**
     * 规则匹配通过
     */
    public final static String MATCHED = "matched";



    /**
     * 规则匹配不通过
     */
    public final static String UNMATCH = "unmatch";


    /**
     * 规则匹配成功, 标记位
     */
    public final static String YES = "Y";


    /**
     * 规则匹配不成功, 标记位
     */
    public final static String NO = "N";


    /**
     * 强制手动填写参数执行规则
     */
    public final static String SELF_EXECUTION = "X";


    /**
     * 规则执行策略：执行所有
     */
    public final static String EXECUTE_ALL = "ALL";


    /**
     * 规则执行策略：执行最新
     */
    public final static String EXECUTE_NEW = "NEW";


    /**
     * Kafka Group 读取位置, 从头开始
     */
    public final static String OFFSET_RESET_EARLIEST = "EARLIEST";


    /**
     * Kafka Group 读取位置, TODO 从最新的位置开始
     */
    public final static String OFFSET_RESET_LATEST = "CURRENT";

    /**
     * Kafka Group 读取位置, 从最后一次的断点开始
     */
    public final static String OFFSET_RESET_CURRENT = "LASTEST";


    /**
     * Topic, 进度发送到该 Topic
     */
    public final static String JOB_PROCESS_TOPIC = "JOB_PROCESS_TOPIC";




    /**
     * Task 统计信息发送到该Topic，数据量统计，任务JMX监控
     */
    public final static String TASK_METRICS_QUEUE = "TASK_METRICS_QUEUE";





    /**
     * 告警分类：Kafka 服务不可用
     */
    public final static String WARN_CATALOG_KAFKA_STATE_INVALID = "kafka_state_error";


    /**
     * 告警分类：Kafka 长时间没有消息
     */
    public final static String WARN_CATALOG_MESSAGE_TIMEOUT = "kafka_no_message";


    /**
     * 告警分类：消息重复
     */
    public final static String WARN_CATALOG_MESSAGE_REPEATED = "kafka_message_repeat";


    /**
     * 告警分类：消息格式不正确
     */
    public final static String WARN_CATALOG_MESSAGE_INVALID = "kafka_message_invalid";


    /**
     * 告警分类：任务时间过长
     */
    public final static String WARN_CATALOG_TASK_TIMEOUT = "task_execute_timeout";


    /**
     * 告警分类：任务执行失败
     */
    public final static String WARN_CATALOG_TASK_EXE_FAILED = "task_execute_failed";


    /**
     * 告警后再继续告警的频率
     */
    public final static String WARN_TASK_TIMEOUT_INTERVAL = "task_warn_timeout_interval";


    /**
     * 告警分类：规则引擎触发超时
     */
    public final static String WARN_CATALOG_RULE_NOTIFY_TIMEOUT = "rule_notify_timeout";


}
