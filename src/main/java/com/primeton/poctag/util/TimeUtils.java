package com.primeton.poctag.util;


import org.apache.commons.lang.StringUtils;

import java.util.Arrays;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

/**
 * <pre>
 *
 * Created by zhaopx.
 * User: zhaopx
 * Date: 2019/4/11
 * Time: 15:02
 *
 * </pre>
 *
 * @author zhaopx
 */
public class TimeUtils {


    /**
     * 1 小时
     */
    public final static long ONE_DAY = 24 * 60 * 60 * 1000L;

    /**
     * 1 小时
     */
    public final static long ONE_HOUR = 60 * 60 * 1000L;


    /**
     * 1 分钟
     */
    public final static long ONE_MINUTE = 60 * 1000L;


    /**
     * 1 s
     */
    public final static long ONE_SECOND = 1000L;


    /**
     * 如果 time is null，""， "  " 返回 -1.
     *
     * 如果 非法字符，抛出 数值无法转换的异常
     *
     * @param time
     * @return
     */
    public static long getTime(String time) {
        time = StringUtils.trim(time);
        if(StringUtils.isBlank(time)) {
            return -1L;
        }

        // 2:10:5 的情况
        if(time.contains(":")){
            if(time.split(":").length == 2) {
                // 如 2:23 的情况，作为分钟和秒的情况
                time = ("0:" + time);
            }
            time = (time + "s");
            time = time.replaceFirst(":", "h");
            time = time.replaceFirst(":", "m");
        }
        if(time.contains("时") || time.contains("分") || time.contains("秒")){
            time = time.replaceFirst("天", "d");
            time = time.replaceFirst("时|小时", "h");
            time = time.replaceFirst("分", "m");
            time = time.replaceFirst("秒", "s");
        }

        String[] split = time.split("\\d+", -1);
        List<String> collect = Arrays.stream(split).filter(it->StringUtils.isNotBlank(it)).collect(Collectors.toList());
        long timeMillis = 0;
        for (int i = 0; i < collect.size(); i++) {
            String unit = collect.get(i);
            int i1 = time.indexOf(unit);
            if(i > 0) {
                String unitx = collect.get(i-1); // 上一个单位
                int i2 = time.indexOf(unitx);
                String t = StringUtils.trim(time.substring(i2+1, i1));
                timeMillis += getSingleUnitTime(t+unit);
            } else {
                String t = StringUtils.trim(time.substring(0, i1));
                timeMillis += getSingleUnitTime(t+unit);
            }
        }
        return timeMillis;
    }


    /**
     * 如果 time is null，""， "  " 返回 -1.
     *
     * 如果 非法字符，抛出 数值无法转换的异常
     *
     * @param time
     * @return
     */
    public static long getSingleUnitTime(String time) {
        time = StringUtils.trim(time);
        if(StringUtils.isBlank(time)) {
            return -1L;
        }

        // 时间的单位
        String unit = time.substring(time.length()-1);

        // 时间值
        String num = time.substring(0, time.length()-1);
        switch (unit) {
            case "S":
            case "s":
                return TimeUnit.SECONDS.toMillis(Integer.parseInt(num));
            case "M":
            case "m":
                return TimeUnit.MINUTES.toMillis(Integer.parseInt(num));
            case "h":
            case "H":
                return TimeUnit.HOURS.toMillis(Integer.parseInt(num));
            case "d":
            case "D":
                return TimeUnit.DAYS.toMillis(Integer.parseInt(num));
            default:
                // 如果没有单位，那就是 ms 了，直接使用 毫秒
                return Long.parseLong(time);
        }
    }

    /**
     * 根据时间戳判断
     * @param timeout
     * @return
     */
    public static String getTimeString(long timeout) {
        long t = timeout / 1000;
        if(t <= 0){
            return "0秒";
        } else if(t <= 60) {
            return t + "秒";
        } else if(t <= 3600) {
            long sec = t % 60;
            return (t / 60) + "分" + sec + "秒";
        }

        long hour = t / 3600;
        long m = (t - hour * 3600) / 60;
        long sec = t % 60;
        return hour + "小时" + m + "分" + sec + "秒";
    }


    /**
     * 返回时间和当前时间相差的时间字符串
     * @param time
     * @return
     */
    public static String getTimeDiffString(long time, long stopTime) {
        long t = (stopTime - time) / 1000;
        if(t <= 0){
            return "0秒";
        } else if(t <= 60) {
            return t + "秒";
        } else if(t <= 3600) {
            long sec = t % 60;
            return (t / 60) + "分" + sec + "秒";
        }

        long hour = t / 3600;
        long m = (t - hour * 3600) / 60;
        long sec = t % 60;
        return hour + "小时" + m + "分" + sec + "秒";
    }
}
