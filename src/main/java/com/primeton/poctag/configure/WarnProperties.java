package com.primeton.poctag.configure;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

/**
 *
 *
 * 提交的任务告警配置
 * @author: zhaopx
 *
 */

@Configuration
@ConfigurationProperties(prefix = "datainsight.warn")
@Getter
@Setter
public class WarnProperties {

    /**
     * 告警 URL，该 URL 应支持 application/json 的消息接收。
     */
    String url;


    /**
     * 是否告警
     */
    String warnEnable = "N";


    /**
     * 任务运行超时告警，超时时间。 支持 h小时，m分，s秒。如 5m  5分钟、 1h 1小时
     */
    String warnJobTimeout;


    /**
     * 任务超时告警后，每个告警消息的间隔。支持 h小时，m分，s秒。如 5m  5分钟、 1h 1小时
     */
    String warnJobPeriod;


    /**
     * 是否开启告警， Y,y,yes,true 为告警， 其他值为不告警
     * @return
     */
    public boolean isWarnEnable() {
        return "Y".equalsIgnoreCase(warnEnable) || "true".equals(warnEnable) | "yes".equals(warnEnable);
    }

}
