package com.primeton.poctag.configure;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

/**
 *
 *
 * 提交的特征任务配置
 * master: local[2]
 *     deployMode: local
 *     appName: HelloTest
 *     executorMemGb: 2
 *     numExecutors: 2
 *     executorCores: 1
 *     queue: default
 *     files: jar
 *     jvmopts: -server
 *     parameters: for=bar
 * @author: zhaopx
 *
 */

@Configuration
@ConfigurationProperties(prefix = "datainsight.spark")
@Getter
@Setter
public class TaskProperties {

    /**
     * 并发默认 3 个
     */
    int parallel = 3;


    /**
     * 并发变量
     */
    private String jvmOpts;


    /**
     * --server -Dparam=val
     */
    private String sparkSubmitOpts;


    /**
     * Spark 任务输出的日志
     */
    private String taskLogDir;

}
