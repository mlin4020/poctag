package com.primeton.poctag.common;

import lombok.Data;

@Data
public class Hits {

    private String _index;
    private String _type;
    private String _source;

}
