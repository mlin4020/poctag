package com.primeton.poctag.common;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.io.Serializable;

@Data
@AllArgsConstructor
public class CommonReturn implements Serializable {
    private Object data;
    private int total;
}
