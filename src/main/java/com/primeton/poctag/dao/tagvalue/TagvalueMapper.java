package com.primeton.poctag.dao.tagvalue;

import com.primeton.poctag.dao.tagvalue.model.Tagvalue;
import com.primeton.poctag.dao.tagvalue.model.TagvalueExample;
import org.apache.ibatis.annotations.Param;
  
import java.util.List;

public interface TagvalueMapper {
    int countByExample(TagvalueExample example);

    int deleteByExample(TagvalueExample example);

    int deleteByPrimaryKey(String id);

    int insert(Tagvalue record);

    int insertSelective(Tagvalue record);

    List<Tagvalue> selectByExample(TagvalueExample example);

    Tagvalue selectByPrimaryKey(String id);

    int updateByExampleSelective(@Param("record") Tagvalue record, @Param("example") TagvalueExample example);

    int updateByExample(@Param("record") Tagvalue record, @Param("example") TagvalueExample example);

    int updateByPrimaryKeySelective(Tagvalue record);

    int updateByPrimaryKey(Tagvalue record);
}
