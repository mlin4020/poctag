package com.primeton.poctag.dao.tagvalue.model;

import java.util.ArrayList;
import java.util.List;

public class TagvalueExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    public TagvalueExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }
        public Criteria andIdIsNull() {
            addCriterion("id is null");
            return (Criteria) this;
        }

        public Criteria andIdIsNotNull() {
            addCriterion("id is not null");
            return (Criteria) this;
        }

        public Criteria andIdEqualTo(String value) {
            addCriterion("id =", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotEqualTo(String value) {
            addCriterion("id <>", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThan(String value) {
            addCriterion("id >", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThanOrEqualTo(String value) {
            addCriterion("id >=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThan(String value) {
            addCriterion("id <", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThanOrEqualTo(String value) {
            addCriterion("id <=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLike(String value) {
            addCriterion("id like", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotLike(String value) {
            addCriterion("id not like", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdIn(List<String> values) {
            addCriterion("id in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotIn(List<String> values) {
            addCriterion("id not in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdBetween(String value1, String value2) {
            addCriterion("id between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotBetween(String value1, String value2) {
            addCriterion("id not between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andValueIsNull() {
            addCriterion("value is null");
            return (Criteria) this;
        }

        public Criteria andValueIsNotNull() {
            addCriterion("value is not null");
            return (Criteria) this;
        }

        public Criteria andValueEqualTo(String value) {
            addCriterion("value =", value, "value");
            return (Criteria) this;
        }

        public Criteria andValueNotEqualTo(String value) {
            addCriterion("value <>", value, "value");
            return (Criteria) this;
        }

        public Criteria andValueGreaterThan(String value) {
            addCriterion("value >", value, "value");
            return (Criteria) this;
        }

        public Criteria andValueGreaterThanOrEqualTo(String value) {
            addCriterion("value >=", value, "value");
            return (Criteria) this;
        }

        public Criteria andValueLessThan(String value) {
            addCriterion("value <", value, "value");
            return (Criteria) this;
        }

        public Criteria andValueLessThanOrEqualTo(String value) {
            addCriterion("value <=", value, "value");
            return (Criteria) this;
        }

        public Criteria andValueLike(String value) {
            addCriterion("value like", value, "value");
            return (Criteria) this;
        }

        public Criteria andValueNotLike(String value) {
            addCriterion("value not like", value, "value");
            return (Criteria) this;
        }

        public Criteria andValueIn(List<String> values) {
            addCriterion("value in", values, "value");
            return (Criteria) this;
        }

        public Criteria andValueNotIn(List<String> values) {
            addCriterion("value not in", values, "value");
            return (Criteria) this;
        }

        public Criteria andValueBetween(String value1, String value2) {
            addCriterion("value between", value1, value2, "value");
            return (Criteria) this;
        }

        public Criteria andValueNotBetween(String value1, String value2) {
            addCriterion("value not between", value1, value2, "value");
            return (Criteria) this;
        }

        public Criteria andLv1IsNull() {
            addCriterion("lv1 is null");
            return (Criteria) this;
        }

        public Criteria andLv1IsNotNull() {
            addCriterion("lv1 is not null");
            return (Criteria) this;
        }

        public Criteria andLv1EqualTo(String value) {
            addCriterion("lv1 =", value, "lv1");
            return (Criteria) this;
        }

        public Criteria andLv1NotEqualTo(String value) {
            addCriterion("lv1 <>", value, "lv1");
            return (Criteria) this;
        }

        public Criteria andLv1GreaterThan(String value) {
            addCriterion("lv1 >", value, "lv1");
            return (Criteria) this;
        }

        public Criteria andLv1GreaterThanOrEqualTo(String value) {
            addCriterion("lv1 >=", value, "lv1");
            return (Criteria) this;
        }

        public Criteria andLv1LessThan(String value) {
            addCriterion("lv1 <", value, "lv1");
            return (Criteria) this;
        }

        public Criteria andLv1LessThanOrEqualTo(String value) {
            addCriterion("lv1 <=", value, "lv1");
            return (Criteria) this;
        }

        public Criteria andLv1Like(String value) {
            addCriterion("lv1 like", value, "lv1");
            return (Criteria) this;
        }

        public Criteria andLv1NotLike(String value) {
            addCriterion("lv1 not like", value, "lv1");
            return (Criteria) this;
        }

        public Criteria andLv1In(List<String> values) {
            addCriterion("lv1 in", values, "lv1");
            return (Criteria) this;
        }

        public Criteria andLv1NotIn(List<String> values) {
            addCriterion("lv1 not in", values, "lv1");
            return (Criteria) this;
        }

        public Criteria andLv1Between(String value1, String value2) {
            addCriterion("lv1 between", value1, value2, "lv1");
            return (Criteria) this;
        }

        public Criteria andLv1NotBetween(String value1, String value2) {
            addCriterion("lv1 not between", value1, value2, "lv1");
            return (Criteria) this;
        }

        public Criteria andLv2IsNull() {
            addCriterion("lv2 is null");
            return (Criteria) this;
        }

        public Criteria andLv2IsNotNull() {
            addCriterion("lv2 is not null");
            return (Criteria) this;
        }

        public Criteria andLv2EqualTo(String value) {
            addCriterion("lv2 =", value, "lv2");
            return (Criteria) this;
        }

        public Criteria andLv2NotEqualTo(String value) {
            addCriterion("lv2 <>", value, "lv2");
            return (Criteria) this;
        }

        public Criteria andLv2GreaterThan(String value) {
            addCriterion("lv2 >", value, "lv2");
            return (Criteria) this;
        }

        public Criteria andLv2GreaterThanOrEqualTo(String value) {
            addCriterion("lv2 >=", value, "lv2");
            return (Criteria) this;
        }

        public Criteria andLv2LessThan(String value) {
            addCriterion("lv2 <", value, "lv2");
            return (Criteria) this;
        }

        public Criteria andLv2LessThanOrEqualTo(String value) {
            addCriterion("lv2 <=", value, "lv2");
            return (Criteria) this;
        }

        public Criteria andLv2Like(String value) {
            addCriterion("lv2 like", value, "lv2");
            return (Criteria) this;
        }

        public Criteria andLv2NotLike(String value) {
            addCriterion("lv2 not like", value, "lv2");
            return (Criteria) this;
        }

        public Criteria andLv2In(List<String> values) {
            addCriterion("lv2 in", values, "lv2");
            return (Criteria) this;
        }

        public Criteria andLv2NotIn(List<String> values) {
            addCriterion("lv2 not in", values, "lv2");
            return (Criteria) this;
        }

        public Criteria andLv2Between(String value1, String value2) {
            addCriterion("lv2 between", value1, value2, "lv2");
            return (Criteria) this;
        }

        public Criteria andLv2NotBetween(String value1, String value2) {
            addCriterion("lv2 not between", value1, value2, "lv2");
            return (Criteria) this;
        }

        public Criteria andLv3IsNull() {
            addCriterion("lv3 is null");
            return (Criteria) this;
        }

        public Criteria andLv3IsNotNull() {
            addCriterion("lv3 is not null");
            return (Criteria) this;
        }

        public Criteria andLv3EqualTo(String value) {
            addCriterion("lv3 =", value, "lv3");
            return (Criteria) this;
        }

        public Criteria andLv3NotEqualTo(String value) {
            addCriterion("lv3 <>", value, "lv3");
            return (Criteria) this;
        }

        public Criteria andLv3GreaterThan(String value) {
            addCriterion("lv3 >", value, "lv3");
            return (Criteria) this;
        }

        public Criteria andLv3GreaterThanOrEqualTo(String value) {
            addCriterion("lv3 >=", value, "lv3");
            return (Criteria) this;
        }

        public Criteria andLv3LessThan(String value) {
            addCriterion("lv3 <", value, "lv3");
            return (Criteria) this;
        }

        public Criteria andLv3LessThanOrEqualTo(String value) {
            addCriterion("lv3 <=", value, "lv3");
            return (Criteria) this;
        }

        public Criteria andLv3Like(String value) {
            addCriterion("lv3 like", value, "lv3");
            return (Criteria) this;
        }

        public Criteria andLv3NotLike(String value) {
            addCriterion("lv3 not like", value, "lv3");
            return (Criteria) this;
        }

        public Criteria andLv3In(List<String> values) {
            addCriterion("lv3 in", values, "lv3");
            return (Criteria) this;
        }

        public Criteria andLv3NotIn(List<String> values) {
            addCriterion("lv3 not in", values, "lv3");
            return (Criteria) this;
        }

        public Criteria andLv3Between(String value1, String value2) {
            addCriterion("lv3 between", value1, value2, "lv3");
            return (Criteria) this;
        }

        public Criteria andLv3NotBetween(String value1, String value2) {
            addCriterion("lv3 not between", value1, value2, "lv3");
            return (Criteria) this;
        }

    }

    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}
