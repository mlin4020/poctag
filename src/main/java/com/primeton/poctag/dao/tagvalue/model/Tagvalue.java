package com.primeton.poctag.dao.tagvalue.model;

import lombok.Data;

/**
 * POC标签表
 * @author 李功林
 * @date 2021/08/20 13:20
 */
@Data
public class Tagvalue {

    private String id;

    private String value;

    private String lv1;

    private String lv2;

    private String lv3;

}
