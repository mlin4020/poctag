package com.primeton.poctag.dao.tag.model;

import lombok.Data;

/**
 * POC标签表
 * @author 李功林
 * @date 2021/08/20 13:08
 */
@Data
public class Tag {

    private String attrid;

    private String attrname;

    private String attrrank;

    private String superid;

    private String roleid;

    private String fieldname;

    private String valuetype;

}
