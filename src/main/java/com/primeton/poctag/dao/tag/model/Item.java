package com.primeton.poctag.dao.tag.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.elasticsearch.annotations.Document;
import org.springframework.data.elasticsearch.annotations.Field;
import org.springframework.data.elasticsearch.annotations.FieldType;

@Data
@AllArgsConstructor
@NoArgsConstructor

@Document(indexName = "user_labels",type = "user_labels", shards = 1, replicas = 0)
public class Item {
    @Id
    private String client_id;

    @Field(store = true, type = FieldType.Keyword)
    private String client_name; //标题
    
    @Field(store = true, type = FieldType.Keyword)
    private String channel_type;// 分类
    
    @Field(store = true, type = FieldType.Keyword)
    private String client_product; // 品牌
    
    @Field(store = true, type = FieldType.Keyword)
    private Double client_phone; // 价格
    
    @Field(store = true, type = FieldType.Integer)
    private String last_datediff; // 图片地址
}