package com.primeton.poctag.dao.tag.model;

import lombok.Data;

import java.util.List;

/**
 * POC标签表
 * @author 李功林
 * @date 2021/08/20 10:14
 */
@Data
public class TagExp extends Tag {

    private List<TagExp> children;

}
