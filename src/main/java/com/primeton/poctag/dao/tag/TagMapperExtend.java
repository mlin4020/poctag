package com.primeton.poctag.dao.tag;

import com.primeton.poctag.dao.tag.model.Tag;
import com.primeton.poctag.dao.tag.model.TagExp;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface TagMapperExtend extends TagMapper {

    List<TagExp> selectAll();

    List<TagExp> selectBySuperId(@Param("superId") String superId);
}
