package com.primeton.poctag.dao.tag.model;

import java.util.ArrayList;
import java.util.List;

public class TagExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    public TagExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }
        public Criteria andAttridIsNull() {
            addCriterion("attrid is null");
            return (Criteria) this;
        }

        public Criteria andAttridIsNotNull() {
            addCriterion("attrid is not null");
            return (Criteria) this;
        }

        public Criteria andAttridEqualTo(String value) {
            addCriterion("attrid =", value, "attrid");
            return (Criteria) this;
        }

        public Criteria andAttridNotEqualTo(String value) {
            addCriterion("attrid <>", value, "attrid");
            return (Criteria) this;
        }

        public Criteria andAttridGreaterThan(String value) {
            addCriterion("attrid >", value, "attrid");
            return (Criteria) this;
        }

        public Criteria andAttridGreaterThanOrEqualTo(String value) {
            addCriterion("attrid >=", value, "attrid");
            return (Criteria) this;
        }

        public Criteria andAttridLessThan(String value) {
            addCriterion("attrid <", value, "attrid");
            return (Criteria) this;
        }

        public Criteria andAttridLessThanOrEqualTo(String value) {
            addCriterion("attrid <=", value, "attrid");
            return (Criteria) this;
        }

        public Criteria andAttridLike(String value) {
            addCriterion("attrid like", value, "attrid");
            return (Criteria) this;
        }

        public Criteria andAttridNotLike(String value) {
            addCriterion("attrid not like", value, "attrid");
            return (Criteria) this;
        }

        public Criteria andAttridIn(List<String> values) {
            addCriterion("attrid in", values, "attrid");
            return (Criteria) this;
        }

        public Criteria andAttridNotIn(List<String> values) {
            addCriterion("attrid not in", values, "attrid");
            return (Criteria) this;
        }

        public Criteria andAttridBetween(String value1, String value2) {
            addCriterion("attrid between", value1, value2, "attrid");
            return (Criteria) this;
        }

        public Criteria andAttridNotBetween(String value1, String value2) {
            addCriterion("attrid not between", value1, value2, "attrid");
            return (Criteria) this;
        }

        public Criteria andAttrnameIsNull() {
            addCriterion("attrname is null");
            return (Criteria) this;
        }

        public Criteria andAttrnameIsNotNull() {
            addCriterion("attrname is not null");
            return (Criteria) this;
        }

        public Criteria andAttrnameEqualTo(String value) {
            addCriterion("attrname =", value, "attrname");
            return (Criteria) this;
        }

        public Criteria andAttrnameNotEqualTo(String value) {
            addCriterion("attrname <>", value, "attrname");
            return (Criteria) this;
        }

        public Criteria andAttrnameGreaterThan(String value) {
            addCriterion("attrname >", value, "attrname");
            return (Criteria) this;
        }

        public Criteria andAttrnameGreaterThanOrEqualTo(String value) {
            addCriterion("attrname >=", value, "attrname");
            return (Criteria) this;
        }

        public Criteria andAttrnameLessThan(String value) {
            addCriterion("attrname <", value, "attrname");
            return (Criteria) this;
        }

        public Criteria andAttrnameLessThanOrEqualTo(String value) {
            addCriterion("attrname <=", value, "attrname");
            return (Criteria) this;
        }

        public Criteria andAttrnameLike(String value) {
            addCriterion("attrname like", value, "attrname");
            return (Criteria) this;
        }

        public Criteria andAttrnameNotLike(String value) {
            addCriterion("attrname not like", value, "attrname");
            return (Criteria) this;
        }

        public Criteria andAttrnameIn(List<String> values) {
            addCriterion("attrname in", values, "attrname");
            return (Criteria) this;
        }

        public Criteria andAttrnameNotIn(List<String> values) {
            addCriterion("attrname not in", values, "attrname");
            return (Criteria) this;
        }

        public Criteria andAttrnameBetween(String value1, String value2) {
            addCriterion("attrname between", value1, value2, "attrname");
            return (Criteria) this;
        }

        public Criteria andAttrnameNotBetween(String value1, String value2) {
            addCriterion("attrname not between", value1, value2, "attrname");
            return (Criteria) this;
        }

        public Criteria andAttrrankIsNull() {
            addCriterion("attrrank is null");
            return (Criteria) this;
        }

        public Criteria andAttrrankIsNotNull() {
            addCriterion("attrrank is not null");
            return (Criteria) this;
        }

        public Criteria andAttrrankEqualTo(String value) {
            addCriterion("attrrank =", value, "attrrank");
            return (Criteria) this;
        }

        public Criteria andAttrrankNotEqualTo(String value) {
            addCriterion("attrrank <>", value, "attrrank");
            return (Criteria) this;
        }

        public Criteria andAttrrankGreaterThan(String value) {
            addCriterion("attrrank >", value, "attrrank");
            return (Criteria) this;
        }

        public Criteria andAttrrankGreaterThanOrEqualTo(String value) {
            addCriterion("attrrank >=", value, "attrrank");
            return (Criteria) this;
        }

        public Criteria andAttrrankLessThan(String value) {
            addCriterion("attrrank <", value, "attrrank");
            return (Criteria) this;
        }

        public Criteria andAttrrankLessThanOrEqualTo(String value) {
            addCriterion("attrrank <=", value, "attrrank");
            return (Criteria) this;
        }

        public Criteria andAttrrankLike(String value) {
            addCriterion("attrrank like", value, "attrrank");
            return (Criteria) this;
        }

        public Criteria andAttrrankNotLike(String value) {
            addCriterion("attrrank not like", value, "attrrank");
            return (Criteria) this;
        }

        public Criteria andAttrrankIn(List<String> values) {
            addCriterion("attrrank in", values, "attrrank");
            return (Criteria) this;
        }

        public Criteria andAttrrankNotIn(List<String> values) {
            addCriterion("attrrank not in", values, "attrrank");
            return (Criteria) this;
        }

        public Criteria andAttrrankBetween(String value1, String value2) {
            addCriterion("attrrank between", value1, value2, "attrrank");
            return (Criteria) this;
        }

        public Criteria andAttrrankNotBetween(String value1, String value2) {
            addCriterion("attrrank not between", value1, value2, "attrrank");
            return (Criteria) this;
        }

        public Criteria andSuperidIsNull() {
            addCriterion("superid is null");
            return (Criteria) this;
        }

        public Criteria andSuperidIsNotNull() {
            addCriterion("superid is not null");
            return (Criteria) this;
        }

        public Criteria andSuperidEqualTo(String value) {
            addCriterion("superid =", value, "superid");
            return (Criteria) this;
        }

        public Criteria andSuperidNotEqualTo(String value) {
            addCriterion("superid <>", value, "superid");
            return (Criteria) this;
        }

        public Criteria andSuperidGreaterThan(String value) {
            addCriterion("superid >", value, "superid");
            return (Criteria) this;
        }

        public Criteria andSuperidGreaterThanOrEqualTo(String value) {
            addCriterion("superid >=", value, "superid");
            return (Criteria) this;
        }

        public Criteria andSuperidLessThan(String value) {
            addCriterion("superid <", value, "superid");
            return (Criteria) this;
        }

        public Criteria andSuperidLessThanOrEqualTo(String value) {
            addCriterion("superid <=", value, "superid");
            return (Criteria) this;
        }

        public Criteria andSuperidLike(String value) {
            addCriterion("superid like", value, "superid");
            return (Criteria) this;
        }

        public Criteria andSuperidNotLike(String value) {
            addCriterion("superid not like", value, "superid");
            return (Criteria) this;
        }

        public Criteria andSuperidIn(List<String> values) {
            addCriterion("superid in", values, "superid");
            return (Criteria) this;
        }

        public Criteria andSuperidNotIn(List<String> values) {
            addCriterion("superid not in", values, "superid");
            return (Criteria) this;
        }

        public Criteria andSuperidBetween(String value1, String value2) {
            addCriterion("superid between", value1, value2, "superid");
            return (Criteria) this;
        }

        public Criteria andSuperidNotBetween(String value1, String value2) {
            addCriterion("superid not between", value1, value2, "superid");
            return (Criteria) this;
        }

        public Criteria andRoleidIsNull() {
            addCriterion("roleid is null");
            return (Criteria) this;
        }

        public Criteria andRoleidIsNotNull() {
            addCriterion("roleid is not null");
            return (Criteria) this;
        }

        public Criteria andRoleidEqualTo(String value) {
            addCriterion("roleid =", value, "roleid");
            return (Criteria) this;
        }

        public Criteria andRoleidNotEqualTo(String value) {
            addCriterion("roleid <>", value, "roleid");
            return (Criteria) this;
        }

        public Criteria andRoleidGreaterThan(String value) {
            addCriterion("roleid >", value, "roleid");
            return (Criteria) this;
        }

        public Criteria andRoleidGreaterThanOrEqualTo(String value) {
            addCriterion("roleid >=", value, "roleid");
            return (Criteria) this;
        }

        public Criteria andRoleidLessThan(String value) {
            addCriterion("roleid <", value, "roleid");
            return (Criteria) this;
        }

        public Criteria andRoleidLessThanOrEqualTo(String value) {
            addCriterion("roleid <=", value, "roleid");
            return (Criteria) this;
        }

        public Criteria andRoleidLike(String value) {
            addCriterion("roleid like", value, "roleid");
            return (Criteria) this;
        }

        public Criteria andRoleidNotLike(String value) {
            addCriterion("roleid not like", value, "roleid");
            return (Criteria) this;
        }

        public Criteria andRoleidIn(List<String> values) {
            addCriterion("roleid in", values, "roleid");
            return (Criteria) this;
        }

        public Criteria andRoleidNotIn(List<String> values) {
            addCriterion("roleid not in", values, "roleid");
            return (Criteria) this;
        }

        public Criteria andRoleidBetween(String value1, String value2) {
            addCriterion("roleid between", value1, value2, "roleid");
            return (Criteria) this;
        }

        public Criteria andRoleidNotBetween(String value1, String value2) {
            addCriterion("roleid not between", value1, value2, "roleid");
            return (Criteria) this;
        }

        public Criteria andFieldnameIsNull() {
            addCriterion("fieldname is null");
            return (Criteria) this;
        }

        public Criteria andFieldnameIsNotNull() {
            addCriterion("fieldname is not null");
            return (Criteria) this;
        }

        public Criteria andFieldnameEqualTo(String value) {
            addCriterion("fieldname =", value, "fieldname");
            return (Criteria) this;
        }

        public Criteria andFieldnameNotEqualTo(String value) {
            addCriterion("fieldname <>", value, "fieldname");
            return (Criteria) this;
        }

        public Criteria andFieldnameGreaterThan(String value) {
            addCriterion("fieldname >", value, "fieldname");
            return (Criteria) this;
        }

        public Criteria andFieldnameGreaterThanOrEqualTo(String value) {
            addCriterion("fieldname >=", value, "fieldname");
            return (Criteria) this;
        }

        public Criteria andFieldnameLessThan(String value) {
            addCriterion("fieldname <", value, "fieldname");
            return (Criteria) this;
        }

        public Criteria andFieldnameLessThanOrEqualTo(String value) {
            addCriterion("fieldname <=", value, "fieldname");
            return (Criteria) this;
        }

        public Criteria andFieldnameLike(String value) {
            addCriterion("fieldname like", value, "fieldname");
            return (Criteria) this;
        }

        public Criteria andFieldnameNotLike(String value) {
            addCriterion("fieldname not like", value, "fieldname");
            return (Criteria) this;
        }

        public Criteria andFieldnameIn(List<String> values) {
            addCriterion("fieldname in", values, "fieldname");
            return (Criteria) this;
        }

        public Criteria andFieldnameNotIn(List<String> values) {
            addCriterion("fieldname not in", values, "fieldname");
            return (Criteria) this;
        }

        public Criteria andFieldnameBetween(String value1, String value2) {
            addCriterion("fieldname between", value1, value2, "fieldname");
            return (Criteria) this;
        }

        public Criteria andFieldnameNotBetween(String value1, String value2) {
            addCriterion("fieldname not between", value1, value2, "fieldname");
            return (Criteria) this;
        }

        public Criteria andValuetypeIsNull() {
            addCriterion("valuetype is null");
            return (Criteria) this;
        }

        public Criteria andValuetypeIsNotNull() {
            addCriterion("valuetype is not null");
            return (Criteria) this;
        }

        public Criteria andValuetypeEqualTo(String value) {
            addCriterion("valuetype =", value, "valuetype");
            return (Criteria) this;
        }

        public Criteria andValuetypeNotEqualTo(String value) {
            addCriterion("valuetype <>", value, "valuetype");
            return (Criteria) this;
        }

        public Criteria andValuetypeGreaterThan(String value) {
            addCriterion("valuetype >", value, "valuetype");
            return (Criteria) this;
        }

        public Criteria andValuetypeGreaterThanOrEqualTo(String value) {
            addCriterion("valuetype >=", value, "valuetype");
            return (Criteria) this;
        }

        public Criteria andValuetypeLessThan(String value) {
            addCriterion("valuetype <", value, "valuetype");
            return (Criteria) this;
        }

        public Criteria andValuetypeLessThanOrEqualTo(String value) {
            addCriterion("valuetype <=", value, "valuetype");
            return (Criteria) this;
        }

        public Criteria andValuetypeLike(String value) {
            addCriterion("valuetype like", value, "valuetype");
            return (Criteria) this;
        }

        public Criteria andValuetypeNotLike(String value) {
            addCriterion("valuetype not like", value, "valuetype");
            return (Criteria) this;
        }

        public Criteria andValuetypeIn(List<String> values) {
            addCriterion("valuetype in", values, "valuetype");
            return (Criteria) this;
        }

        public Criteria andValuetypeNotIn(List<String> values) {
            addCriterion("valuetype not in", values, "valuetype");
            return (Criteria) this;
        }

        public Criteria andValuetypeBetween(String value1, String value2) {
            addCriterion("valuetype between", value1, value2, "valuetype");
            return (Criteria) this;
        }

        public Criteria andValuetypeNotBetween(String value1, String value2) {
            addCriterion("valuetype not between", value1, value2, "valuetype");
            return (Criteria) this;
        }

    }

    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}
