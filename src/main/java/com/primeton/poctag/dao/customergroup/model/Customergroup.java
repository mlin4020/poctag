package com.primeton.poctag.dao.customergroup.model;

import lombok.Data;
import com.fasterxml.jackson.annotation.JsonFormat;
import java.util.Date;

/**
 * 客群
 * @author 李功林
 * @date 2021/08/24 09:57
 */
@Data
public class Customergroup {

    private Integer id;

    private String groupname;

    private String args;

    private String creater;

    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date createtime;

    private Integer total;

    private String description;

    private String exportstatus;

    private String exportlocation;

    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date exportdate;

}
