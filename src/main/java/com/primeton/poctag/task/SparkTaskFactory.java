package com.primeton.poctag.task;

import com.primeton.poctag.configure.TaskProperties;
import com.primeton.poctag.service.warn.WarnService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

/**
 * <pre>
 *
 * Created by zhaopx.
 * User: zhaopx
 * Date: 2020/11/17
 * Time: 12:54
 *
 * </pre>
 *
 * @author zhaopx
 */
@Component
@Slf4j
public class SparkTaskFactory {


    @Autowired
    TaskProperties taskProperties;


    @Autowired
    WarnService warnService;


    /**
     * 根据 TaskInfo 生成 Task，不同 Task 类型
     * @param taskId TaskInstanceID
     * @param taskInfo
     * @return
     */
    public SparkTask newTask(String taskId, Map taskInfo) {
        return newTask(taskId, (String) taskInfo.get("xmlFile"), taskInfo);
    }


    public SparkTask newTask(String taskId, String xmlFile, Map taskInfo) {
        String type = Optional.ofNullable((String)taskInfo.get("taskType")).orElse("TEST");
        switch (type) {
            case "UG_EXPORT":
                // 相似性分析 Spark 任务
                Map<String, String> taskParameters = new HashMap<>();
                taskParameters.put("taskId", taskId);
                taskParameters.put("queryDsl", (String) taskInfo.get("queryDsl"));
                //taskParameters 需要的参数，这里放入

                ShellNativeLocalTask similarityTask = new ShellNativeLocalTask(
                        "SimilarityAnalyseSparkJob",
                        xmlFile,
                        taskId,
                        taskParameters);
                similarityTask.setTaskProperties(taskProperties);
                similarityTask.setWarnService(warnService);
                return similarityTask;
            default:
                // 测试任务，未知任务
                return new TestTask(taskId);
        }
    }
}
