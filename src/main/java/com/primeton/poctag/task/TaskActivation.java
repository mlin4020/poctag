package com.primeton.poctag.task;

/**
 *
 *
 * 任务消息激活
 *
 * <pre>
 *
 * Created by zhaopx.
 * User: zhaopx
 * Date: 2020/11/17
 * Time: 13:54
 *
 * </pre>
 *
 * @author zhaopx
 */
public interface TaskActivation {


    /**
     * 触发 Task 执行
     */
    public void active();
}
