package com.primeton.poctag.task;

import java.util.Collections;
import java.util.Map;

/**
 *
 * 测试
 * <pre>
 *
 * Created by zhaopx.
 * User: zhaopx
 * Date: 2020/11/17
 * Time: 12:59
 *
 * </pre>
 *
 * @author zhaopx
 */
public class TestTask extends SparkTask {

    public TestTask(String taskId) {
        super(taskId);
    }

    @Override
    public Map<String, Object> doExecute() throws Exception {
        // 开始运行
        Thread.sleep(10 * 1000);
        return Collections.emptyMap();
    }
}
