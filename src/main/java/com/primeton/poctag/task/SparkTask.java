package com.primeton.poctag.task;

import java.util.Map;
import java.util.concurrent.Callable;

/**
 *
 * Spark 任务并发控制，控制总超类
 *
 * <pre>
 *
 * Created by zhaopx.
 * User: zhaopx
 * Date: 2020/11/17
 * Time: 11:11
 *
 * </pre>
 *
 * @author zhaopx
 */
public abstract class SparkTask implements Callable<Map<String, Object>> {


    final String taskId;


    public SparkTask(String taskId) {
        this.taskId = taskId;
    }


    /**
     * 获取前端 TASKID
     *
     * @return
     */
    public final String getTaskId() {
        return taskId;
    }

    /**
     * 执行任务
     */
    @Override
    public final Map<String, Object> call() {
        try {
            return doExecute();
        } catch (Exception e) {
            throw new IllegalStateException(e);
        }
    }


    /**
     * 执行任务
     * @throws Exception
     */
    public abstract Map<String, Object> doExecute() throws Exception;
}
