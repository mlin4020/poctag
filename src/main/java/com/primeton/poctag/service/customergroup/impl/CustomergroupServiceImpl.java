package com.primeton.poctag.service.customergroup.impl;

import com.alibaba.fastjson.JSONArray;
import com.primeton.poctag.api.tag.TagController;
import com.primeton.poctag.core.model.ApiResponse;
import com.primeton.poctag.core.model.PageData;
import com.primeton.poctag.core.model.PageWrap;
import com.primeton.poctag.core.utils.ExampleBuilder;
import com.primeton.poctag.dao.customergroup.CustomergroupMapper;
import com.primeton.poctag.dao.customergroup.model.Customergroup;
import com.primeton.poctag.dao.customergroup.model.CustomergroupExample;
import com.primeton.poctag.dto.QueryDto;
import com.primeton.poctag.service.customergroup.CustomergroupService;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.io.IOException;
import java.util.List;

/**
 * 客群Service实现
 * @author 李功林
 * @date 2021/08/20 10:14
 */
@Service
public class CustomergroupServiceImpl implements CustomergroupService {

    @Autowired
    private CustomergroupMapper customergroupMapper;
    @Autowired
    private TagController tagController;

    @Override
    public Integer create(Customergroup customergroup) throws IOException {
        String args = customergroup.getArgs();
        List<QueryDto> queryDtos = JSONArray.parseArray(args, QueryDto.class);
        ApiResponse count = tagController.count(queryDtos);
        customergroup.setTotal((Integer) count.getData());
        customergroupMapper.insertSelective(customergroup);
        return customergroup.getId();
    }

    @Override
    public void deleteById(Integer id) {
        customergroupMapper.deleteByPrimaryKey(id);
    }

    @Override
    public void deleteByIdInBatch(List<Integer> ids) {
        if (CollectionUtils.isEmpty(ids)) return;
        for (Integer id: ids) {
            this.deleteById(id);
        }
    }

    @Override
    public void updateById(Customergroup customergroup) {
        customergroupMapper.updateByPrimaryKeySelective(customergroup);
    }

    @Override
    public void updateByIdInBatch(List<Customergroup> customergroups) {
        if (CollectionUtils.isEmpty(customergroups)) return;
        for (Customergroup customergroup: customergroups) {
            this.updateById(customergroup);
        }
    }

    @Override
    public Customergroup findById(Integer id) {
        return customergroupMapper.selectByPrimaryKey(id);
    }

    @Override
    public Customergroup findOne(Customergroup customergroup) {
        ExampleBuilder<CustomergroupExample, CustomergroupExample.Criteria> builder = ExampleBuilder.create(CustomergroupExample.class, CustomergroupExample.Criteria.class);
        List<Customergroup> customergroups = customergroupMapper.selectByExample(builder.buildExamplePack(customergroup).getExample());
        if (customergroups.size() > 0) {
            return customergroups.get(0);
        }
        return null;
    }

    @Override
    public List<Customergroup> findList(Customergroup customergroup) {
        ExampleBuilder<CustomergroupExample, CustomergroupExample.Criteria> builder = ExampleBuilder.create(CustomergroupExample.class, CustomergroupExample.Criteria.class);
        return customergroupMapper.selectByExample(builder.buildExamplePack(customergroup).getExample());
    }

    @Override
    public PageData<Customergroup> findPage(PageWrap pageWrap) {
        PageHelper.startPage(pageWrap.getPage(), pageWrap.getCapacity());
        ExampleBuilder<CustomergroupExample, CustomergroupExample.Criteria> builder = ExampleBuilder.create(CustomergroupExample.class, CustomergroupExample.Criteria.class);
        ExampleBuilder.ExamplePack<CustomergroupExample, CustomergroupExample.Criteria> pack = builder.buildExamplePack(pageWrap.getModel());
        pack.getExample().setOrderByClause(pageWrap.getOrderByClause());
        return PageData.from(new PageInfo<>(customergroupMapper.selectByExample(pack.getExample())));
    }
    @Override
    public List<Customergroup> findAll() {
        return customergroupMapper.selectByExample(new CustomergroupExample());
    }

    @Override
    public long count(Customergroup customergroup) {
        ExampleBuilder<CustomergroupExample, CustomergroupExample.Criteria> builder = ExampleBuilder.create(CustomergroupExample.class, CustomergroupExample.Criteria.class);
        return customergroupMapper.countByExample(builder.buildExamplePack(customergroup).getExample());
    }
}
