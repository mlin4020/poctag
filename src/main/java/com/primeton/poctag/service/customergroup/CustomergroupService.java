package com.primeton.poctag.service.customergroup;

import com.primeton.poctag.core.model.PageData;
import com.primeton.poctag.core.model.PageWrap;
import com.primeton.poctag.dao.customergroup.model.Customergroup;

import java.io.IOException;
import java.util.List;

/**
 * 客群Service定义
 * @author 李功林
 * @date 2021/08/20 10:14
 */
public interface CustomergroupService {

    /**
     * 创建
     * @author 李功林
     * @date 2021/08/20 10:14
     */
    Integer create(Customergroup customergroup) throws IOException;

    /**
     * 主键删除
     * @author 李功林
     * @date 2021/08/20 10:14
     */
    void deleteById(Integer id);

    /**
     * 批量主键删除
     * @author 李功林
     * @date 2021/08/20 10:14
     */
    void deleteByIdInBatch(List<Integer> ids);

    /**
     * 主键更新
     * @author 李功林
     * @date 2021/08/20 10:14
     */
    void updateById(Customergroup customergroup);

    /**
     * 批量主键更新
     * @author 李功林
     * @date 2021/08/20 10:14
     */
    void updateByIdInBatch(List<Customergroup> customergroups);

    /**
     * 主键查询
     * @author 李功林
     * @date 2021/08/20 10:14
     */
    Customergroup findById(Integer id);

    /**
     * 条件查询单条记录
     * @author 李功林
     * @date 2021/08/20 10:14
     */
    Customergroup findOne(Customergroup customergroup);

    /**
     * 条件查询
     * @author 李功林
     * @date 2021/08/20 10:14
     */
    List<Customergroup> findList(Customergroup customergroup);

    /**
     * 分页查询
     * @author 李功林
     * @date 2021/08/20 10:14
     */
    PageData<Customergroup> findPage(PageWrap<Customergroup> pageWrap);

    /**
     * 条件统计
     * @author 李功林
     * @date 2021/08/20 10:14
     */
    long count(Customergroup customergroup);

    List<Customergroup> findAll();
}
