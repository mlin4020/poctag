package com.primeton.poctag.service.tagvalue;

import com.primeton.poctag.core.model.PageData;
import com.primeton.poctag.core.model.PageWrap;
import com.primeton.poctag.dao.tagvalue.model.Tagvalue;
import java.util.List;

/**
 * POC标签表Service定义
 * @author 李功林
 * @date 2021/08/20 13:20
 */
public interface TagvalueService {

    /**
     * 创建
     * @author 李功林
     * @date 2021/08/20 13:20
     */
    String create(Tagvalue tagvalue);

    /**
     * 主键删除
     * @author 李功林
     * @date 2021/08/20 13:20
     */
    void deleteById(String id);

    /**
     * 批量主键删除
     * @author 李功林
     * @date 2021/08/20 13:20
     */
    void deleteByIdInBatch(List<String> ids);

    /**
     * 主键更新
     * @author 李功林
     * @date 2021/08/20 13:20
     */
    void updateById(Tagvalue tagvalue);

    /**
     * 批量主键更新
     * @author 李功林
     * @date 2021/08/20 13:20
     */
    void updateByIdInBatch(List<Tagvalue> tagvalues);

    /**
     * 主键查询
     * @author 李功林
     * @date 2021/08/20 13:20
     */
    Tagvalue findById(String id);

    /**
     * 条件查询单条记录
     * @author 李功林
     * @date 2021/08/20 13:20
     */
    Tagvalue findOne(Tagvalue tagvalue);

    /**
     * 条件查询
     * @author 李功林
     * @date 2021/08/20 13:20
     */
    List<Tagvalue> findList(Tagvalue tagvalue);
  
    /**
     * 分页查询
     * @author 李功林
     * @date 2021/08/20 13:20
     */
    PageData<Tagvalue> findPage(PageWrap<Tagvalue> pageWrap);

    /**
     * 条件统计
     * @author 李功林
     * @date 2021/08/20 13:20
     */
    long count(Tagvalue tagvalue);
}
