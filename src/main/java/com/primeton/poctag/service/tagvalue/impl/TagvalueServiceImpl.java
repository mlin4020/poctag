package com.primeton.poctag.service.tagvalue.impl;

import com.primeton.poctag.core.model.PageData;
import com.primeton.poctag.core.model.PageWrap;
import com.primeton.poctag.core.utils.ExampleBuilder;
import com.primeton.poctag.dao.tagvalue.TagvalueMapper;
import com.primeton.poctag.dao.tagvalue.model.Tagvalue;
import com.primeton.poctag.dao.tagvalue.model.TagvalueExample;
import com.primeton.poctag.service.tagvalue.TagvalueService;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.util.List;

/**
 * POC标签表Service实现
 * @author 李功林
 * @date 2021/08/20 13:20
 */
@Service
public class TagvalueServiceImpl implements TagvalueService {

    @Autowired
    private TagvalueMapper tagvalueMapper;

    @Override
    public String create(Tagvalue tagvalue) {
        tagvalueMapper.insertSelective(tagvalue);
        return tagvalue.getId();
    }

    @Override
    public void deleteById(String id) {
        tagvalueMapper.deleteByPrimaryKey(id);
    }

    @Override
    public void deleteByIdInBatch(List<String> ids) {
        if (CollectionUtils.isEmpty(ids)) return;
        for (String id: ids) {
            this.deleteById(id);
        }
    }

    @Override
    public void updateById(Tagvalue tagvalue) {
        tagvalueMapper.updateByPrimaryKeySelective(tagvalue);
    }

    @Override
    public void updateByIdInBatch(List<Tagvalue> tagvalues) {
        if (CollectionUtils.isEmpty(tagvalues)) return;
        for (Tagvalue tagvalue: tagvalues) {
            this.updateById(tagvalue);
        }
    }

    @Override
    public Tagvalue findById(String id) {
        return tagvalueMapper.selectByPrimaryKey(id);
    }

    @Override
    public Tagvalue findOne(Tagvalue tagvalue) {
        ExampleBuilder<TagvalueExample, TagvalueExample.Criteria> builder = ExampleBuilder.create(TagvalueExample.class, TagvalueExample.Criteria.class);
        List<Tagvalue> tagvalues = tagvalueMapper.selectByExample(builder.buildExamplePack(tagvalue).getExample());
        if (tagvalues.size() > 0) {
            return tagvalues.get(0);
        }
        return null;
    }

    @Override
    public List<Tagvalue> findList(Tagvalue tagvalue) {
        ExampleBuilder<TagvalueExample, TagvalueExample.Criteria> builder = ExampleBuilder.create(TagvalueExample.class, TagvalueExample.Criteria.class);
        return tagvalueMapper.selectByExample(builder.buildExamplePack(tagvalue).getExample());
    }
  
    @Override
    public PageData<Tagvalue> findPage(PageWrap pageWrap) {
        PageHelper.startPage(pageWrap.getPage(), pageWrap.getCapacity());
        ExampleBuilder<TagvalueExample, TagvalueExample.Criteria> builder = ExampleBuilder.create(TagvalueExample.class, TagvalueExample.Criteria.class);
        ExampleBuilder.ExamplePack<TagvalueExample, TagvalueExample.Criteria> pack = builder.buildExamplePack(pageWrap.getModel());
        pack.getExample().setOrderByClause(pageWrap.getOrderByClause());
        return PageData.from(new PageInfo<>(tagvalueMapper.selectByExample(pack.getExample())));
    }

    @Override
    public long count(Tagvalue tagvalue) {
        ExampleBuilder<TagvalueExample, TagvalueExample.Criteria> builder = ExampleBuilder.create(TagvalueExample.class, TagvalueExample.Criteria.class);
        return tagvalueMapper.countByExample(builder.buildExamplePack(tagvalue).getExample());
    }
}
