package com.primeton.poctag.service.warn;


import java.util.Map;

/**
 * <pre>
 *
 * Created by zhaopx.
 * User: zhaopx
 * Date: 2019/4/4
 * Time: 09:40
 *
 * </pre>
 *
 * @author zhaopx
 */
public interface WarnService {

    /**
     * 发送一条预警
     * @return 返回发送状态
     */
    public Map<String, Object> warn(WarnMessage warnMessage);


    /**
     * 任务运行超时告警，超时时间。 支持 h小时，m分，s秒。如 5m  5分钟、 1h 1小时；
     * @return
     */
    public long getWarnJobTimeout();


    /**
     * 任务超时告警后，每个告警消息的间隔。支持 h小时，m分，s秒。如 5m  5分钟、 1h 1小时；
     * @return
     */
    public long getWarnJobPeriod();

    /**
     * 是否告警, true 和 Y，yes 为是告警，其他值为 false 不告警；
     * @return
     */
    public boolean isWarnEnable();
}
