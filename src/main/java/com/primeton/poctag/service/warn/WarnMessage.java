package com.primeton.poctag.service.warn;

import com.alibaba.fastjson.JSON;

import java.util.Date;


/**
 * 告警消息实体
 */
public class WarnMessage {
    private Integer warnId;

    private String warnLevel;

    private String warnCatalog;

    private String warnType;

    private Date warnStart;

    private String warnSubtext;

    private String warnContent;

    private String sendUser;

    private Date createdAt;

    public Integer getWarnId() {
        return warnId;
    }

    public void setWarnId(Integer warnId) {
        this.warnId = warnId;
    }

    public String getWarnLevel() {
        return warnLevel;
    }

    public void setWarnLevel(String warnLevel) {
        this.warnLevel = warnLevel == null ? null : warnLevel.trim();
    }

    public String getWarnCatalog() {
        return warnCatalog;
    }

    public void setWarnCatalog(String warnCatalog) {
        this.warnCatalog = warnCatalog == null ? null : warnCatalog.trim();
    }

    public String getWarnType() {
        return warnType;
    }

    public void setWarnType(String warnType) {
        this.warnType = warnType == null ? null : warnType.trim();
    }

    public Date getWarnStart() {
        return warnStart;
    }

    public void setWarnStart(Date warnStart) {
        this.warnStart = warnStart;
    }

    public String getWarnSubtext() {
        return warnSubtext;
    }

    public void setWarnSubtext(String warnSubtext) {
        this.warnSubtext = warnSubtext == null ? null : warnSubtext.trim();
    }

    public String getWarnContent() {
        return warnContent;
    }

    public void setWarnContent(String warnContent) {
        this.warnContent = warnContent == null ? null : warnContent.trim();
    }

    public String getSendUser() {
        return sendUser;
    }

    public void setSendUser(String sendUser) {
        this.sendUser = sendUser == null ? null : sendUser.trim();
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    @Override
    public String toString() {
        return JSON.toJSONString(this);
    }
}