package com.primeton.poctag.service.tag;

import com.primeton.poctag.core.model.PageData;
import com.primeton.poctag.core.model.PageWrap;
import com.primeton.poctag.dao.tag.model.Tag;
import com.primeton.poctag.dao.tag.model.TagExp;

import java.util.List;

/**
 * POC标签表Service定义
 * @author 李功林
 * @date 2021/08/20 10:14
 */
public interface TagService {

    /**
     * 创建
     * @author 李功林
     * @date 2021/08/20 10:14
     */
    String create(Tag tag);

    /**
     * 主键删除
     * @author 李功林
     * @date 2021/08/20 10:14
     */
    void deleteById(String id);

    /**
     * 批量主键删除
     * @author 李功林
     * @date 2021/08/20 10:14
     */
    void deleteByIdInBatch(List<String> ids);

    /**
     * 主键更新
     * @author 李功林
     * @date 2021/08/20 10:14
     */
    void updateById(Tag tag);

    /**
     * 批量主键更新
     * @author 李功林
     * @date 2021/08/20 10:14
     */
    void updateByIdInBatch(List<Tag> tags);

    /**
     * 主键查询
     * @author 李功林
     * @date 2021/08/20 10:14
     */
    Tag findById(String id);

    /**
     * 条件查询单条记录
     * @author 李功林
     * @date 2021/08/20 10:14
     */
    Tag findOne(Tag tag);

    /**
     * 条件查询
     * @author 李功林
     * @date 2021/08/20 10:14
     */
    List<Tag> findList(Tag tag);
  
    /**
     * 分页查询
     * @author 李功林
     * @date 2021/08/20 10:14
     */
    PageData<Tag> findPage(PageWrap<Tag> pageWrap);

    /**
     * 条件统计
     * @author 李功林
     * @date 2021/08/20 10:14
     */
    long count(Tag tag);


    List<TagExp> getTree(String superId);
}
