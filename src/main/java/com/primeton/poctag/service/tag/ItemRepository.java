package com.primeton.poctag.service.tag;

import com.primeton.poctag.dao.tag.model.Item;
import org.elasticsearch.repositories.Repository;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

import java.awt.print.Book;
import java.util.List;
import java.util.Map;
public interface ItemRepository extends ElasticsearchRepository<Item,String> {
}
