package com.primeton.poctag.service.tag.impl;

import com.primeton.poctag.core.model.PageData;
import com.primeton.poctag.core.model.PageWrap;
import com.primeton.poctag.core.utils.ExampleBuilder;
import com.primeton.poctag.dao.tag.TagMapperExtend;
import com.primeton.poctag.dao.tag.model.Tag;
import com.primeton.poctag.dao.tag.model.TagExample;
import com.primeton.poctag.dao.tag.model.TagExp;
import com.primeton.poctag.service.tag.TagService;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * POC标签表Service实现
 * @author 李功林
 * @date 2021/08/20 10:14
 */
@Service
public class TagServiceImpl implements TagService {

    @Autowired
    private TagMapperExtend tagMapper;

    @Override
    public String create(Tag tag) {
        tagMapper.insertSelective(tag);
        return tag.getAttrid();
    }

    @Override
    public void deleteById(String id) {
        tagMapper.deleteByPrimaryKey(id);
    }

    @Override
    public void deleteByIdInBatch(List<String> ids) {
        if (CollectionUtils.isEmpty(ids)) return;
        for (String id: ids) {
            this.deleteById(id);
        }
    }

    @Override
    public void updateById(Tag tag) {
        tagMapper.updateByPrimaryKeySelective(tag);
    }

    @Override
    public void updateByIdInBatch(List<Tag> tags) {
        if (CollectionUtils.isEmpty(tags)) return;
        for (Tag tag: tags) {
            this.updateById(tag);
        }
    }

    @Override
    public Tag findById(String id) {
        return tagMapper.selectByPrimaryKey(id);
    }

    @Override
    public Tag findOne(Tag tag) {
        ExampleBuilder<TagExample, TagExample.Criteria> builder = ExampleBuilder.create(TagExample.class, TagExample.Criteria.class);
        List<Tag> tags = tagMapper.selectByExample(builder.buildExamplePack(tag).getExample());
        if (tags.size() > 0) {
            return tags.get(0);
        }
        return null;
    }

    @Override
    public List<Tag> findList(Tag tag) {
        ExampleBuilder<TagExample, TagExample.Criteria> builder = ExampleBuilder.create(TagExample.class, TagExample.Criteria.class);
        return tagMapper.selectByExample(builder.buildExamplePack(tag).getExample());
    }
  
    @Override
    public PageData<Tag> findPage(PageWrap pageWrap) {
        PageHelper.startPage(pageWrap.getPage(), pageWrap.getCapacity());
        ExampleBuilder<TagExample, TagExample.Criteria> builder = ExampleBuilder.create(TagExample.class, TagExample.Criteria.class);
        ExampleBuilder.ExamplePack<TagExample, TagExample.Criteria> pack = builder.buildExamplePack(pageWrap.getModel());
        pack.getExample().setOrderByClause(pageWrap.getOrderByClause());
        return PageData.from(new PageInfo<>(tagMapper.selectByExample(pack.getExample())));
    }

    @Override
    public long count(Tag tag) {
        ExampleBuilder<TagExample, TagExample.Criteria> builder = ExampleBuilder.create(TagExample.class, TagExample.Criteria.class);
        return tagMapper.countByExample(builder.buildExamplePack(tag).getExample());
    }

    @Override
    public List<TagExp> getTree(String superId) {
        List<TagExp> tags = this.tagMapper.selectAll();
        List<TagExp> tagss = this.getTree(tags,superId);
        return tagss;
    }
    public List<TagExp> getTree(List<TagExp> allData,String superId) {
        List<TagExp> tagExps = this.selectBySuperId(allData, superId);
        for (TagExp tag: tagExps) {
            List<TagExp> tagss = this.getTree(allData,tag.getAttrid());
            if(tagss.size()>0){
                tag.setChildren(tagss);
            }
        }
        return tagExps;
    }
    public List<TagExp> selectBySuperId(List<TagExp> allData, String superId) {
        return allData.stream().filter(item -> {
            return item.getSuperid().equals(superId);
        }).collect(Collectors.toList());
    }
}
