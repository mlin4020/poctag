package com.primeton.poctag.dto;

import lombok.Data;

import java.io.Serializable;
import java.util.List;

@Data
public class QueryDto implements Serializable {
    //字段名
//    private String columnName;
    //条件
//    private String condition;
    //值
//    private List<String> values;
    //两个条件之间的连接关系 and or
//    private String connectType;


    //两个条件之间的连接关系 and or
    private String key;
    private String type;
    private String attrname;


    //字段使用
    private String fieldname;
    private String logicalConnective;
    private List<String> values;
}
