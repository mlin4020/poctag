package com.primeton.poctag.dto;

import com.primeton.poctag.core.model.PageWrap;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

@Data
public class QueryVo extends PageWrap implements Serializable {
   private List<QueryDto> queryDtos;

}
