package com.primeton.poctag.service;

import com.alibaba.fastjson.JSON;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.ComponentScan;

/**
 * 单元测试基类
 * @author 李功林
 * @date 2021/08/19 14:44
 */
@Slf4j
@ComponentScan("com.primeton.poctag")
public class BaseTest {

    /**
     * 打印测试结果
     * @author 李功林
     * @date 2021/08/19 14:44
     */
    protected void print(Object obj) {
        log.info("测试结果: {}", obj == null ? "无返回" : JSON.toJSONString(obj));
    }

}
