//package com.primeton.poctag.service;
//
//import com.primeton.poctag.core.model.PageWrap;
//import com.primeton.poctag.service.tag.ItemRepository;
//import lombok.extern.slf4j.Slf4j;
//import org.elasticsearch.index.query.QueryBuilders;
//import org.junit.Test;
//import org.junit.runner.RunWith;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
//import org.springframework.boot.test.context.SpringBootTest;
//import org.springframework.data.domain.Page;
//import org.springframework.data.elasticsearch.core.query.NativeSearchQueryBuilder;
//import org.springframework.test.context.junit4.SpringRunner;
//import org.springframework.transaction.annotation.Transactional;
//
//import java.util.Arrays;
//
///**
// * 示例单元测试
// * @author 李功林
// * @date 2021/08/19 14:44
// */
//@Slf4j
//@RunWith(SpringRunner.class)
//@EnableAutoConfiguration
//@SpringBootTest(classes = com.primeton.poctag.Application.class)
//@Transactional
//public class UserServiceTest extends BaseTest {
//
//    @Autowired
//    private ItemRepository itemRepository;
//
//    @Test
//    public void createTest() {
//        // 构建查询条件
//        NativeSearchQueryBuilder queryBuilder = new NativeSearchQueryBuilder();
//        // 添加基本分词查询
//        queryBuilder.withQuery(QueryBuilders.matchQuery("client_name", "赵平西"));
//        // 搜索，获取结果
//        Page search = this.itemRepository.search(queryBuilder.build());
//    }
//
//    @Test
//    public void deleteByIdTest() {
//    }
//
//    @Test
//    public void deleteByIdInBatchTest() {
//    }
//
//    @Test
//    public void updateByIdTest() {
//    }
//
//    @Test
//    public void updateByIdInBatchTest() {
//    }
//
//
//}
